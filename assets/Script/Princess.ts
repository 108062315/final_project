
const {ccclass, property} = cc._decorator;

import Global = require("./Global");

@ccclass
export default class NewClass extends cc.Component {
    
    @property(cc.Sprite)
    dialogue: cc.Sprite = null;
    @property(cc.Sprite)
    frame: cc.Sprite = null;
    @property(cc.Sprite)
    roleIcon: cc.Sprite = null;
    

    @property(cc.Label)
    nameLabel: cc.Label = null;
    @property(cc.Label)
    textLabel: cc.Label = null;
    @property(cc.Button)
    Next: cc.Button = null;
    @property(cc.Label)
    NextLabel: cc.Label = null;

    
    @property(cc.Button)
    closeIcon: cc.Button = null;
    @property(cc.Sprite)
    closeBackground: cc.Sprite = null;
    @property(cc.Sprite)
    nextBackground: cc.Sprite = null;

    dialogue_show : boolean = false;
    last_dialogue : boolean = false;
    // LIFE-CYCLE CALLBACKS:

    nameString : string = "公主";
    text : string = "勇者大人......";
    a : string = "";
    url : "Atlas/Braver_atlas/1";
    
    onLoad () {
        this.dialogue.node.height = 0;
        this.frame.node.height = 0;
        this.roleIcon.node.height = 0;
        this.closeIcon.node.height = 0;
        this.nameLabel.node.height = 0;
        this.textLabel.node.height = 0;
        this.Next.node.height = 0;
        this.NextLabel.node.height = 0;

        this.closeBackground.node.height = 0;
        this.nextBackground.node.height = 0;

        this.dialogue_show = false;
        
    }

    update(dt){
        
    }

    Next_Page(){
        //alert("next");
        this.text = "我被章魚大魔王給詛咒了，請救救我......";
        
        this.textLabel.node.getComponent(cc.Label).string = this.text;
        this.last_dialogue = true;
        if(this.last_dialogue){
            //alert("last");
            this.Next.node.height = 0;
            this.nextBackground.node.height = 0;
        }
    }

    close_window(){
        this.nameLabel.node.getComponent(cc.Label).string = "";
        this.textLabel.node.getComponent(cc.Label).string = "";
        this.NextLabel.node.getComponent(cc.Label).string = "";

        this.dialogue.node.height = 0;
        this.frame.node.height = 0;
        this.roleIcon.node.height = 0;
        this.closeIcon.node.height = 0;
        
        this.nameLabel.node.height = 0;
        this.textLabel.node.height = 0;
        this.Next.node.height = 0;
        this.NextLabel.node.height = 0;
        this.closeBackground.node.height = 0;
        this.nextBackground.node.height = 0;
        

        this.dialogue_show = false;
    }
    onBeginContact(contact, self, other){
        //cc.log(other.node.name);
        if(other.node.name == "Braver"){
            //alert(other.node.name);
            
            this.nameLabel.node.getComponent(cc.Label).string = this.nameString;
            this.textLabel.node.getComponent(cc.Label).string = this.text;
            this.NextLabel.node.getComponent(cc.Label).string = this.a;

            this.dialogue.node.height = 150;
            this.frame.node.height = 64;
            this.roleIcon.node.height = 50;
            this.closeIcon.node.height = 32;

            this.closeBackground.node.height = 32;
            this.nextBackground.node.height = 32;
            
            this.nameLabel.node.height = 25;
            this.textLabel.node.height = 40;

            this.Next.node.height = 32;
            this.NextLabel.node.height = 30;

            this.dialogue_show = true;
        }
    }
}
