import Global = require("./Global");

const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends cc.Component {

    @property(cc.Node)
    Main_Camera: cc.Node = null;


    onLoad () {
        switch (Global.enemyname) {
            case "green_slime" : 
                Global.enemyblood = 50;
                Global.enemyattack = 20;
                Global.enemydefense = 1;
                Global.enemyexp = 1;
                Global.enemygold = 1;
                Global.enemyspeed = 3;
                this.node.getComponent(cc.Label).string = "綠頭怪";
                this.node.color = cc.color(255,255,255);
                break;
            case "red_slime" : 
                Global.enemyblood = 70;
                Global.enemyattack = 15;
                Global.enemydefense = 2;
                Global.enemyexp = 2;
                Global.enemygold = 2;
                Global.enemyspeed = 5;
                this.node.getComponent(cc.Label).string = "紅頭怪";
                this.node.color = cc.color(255,255,255);
                break;
            
            case "bat_1" : 
                Global.enemyblood = 100;
                Global.enemyattack = 20;
                Global.enemydefense = 5;
                Global.enemyexp = 3;
                Global.enemygold = 3;
                Global.enemyspeed = 5;
                this.node.getComponent(cc.Label).string = "小蝙蝠";
                this.node.color = cc.color(255,255,255);
                break;

            case "bone_1" : 
                Global.enemyblood = 150;
                Global.enemyattack = 25;
                Global.enemydefense = 5;
                Global.enemyexp = 5;
                Global.enemygold = 4;
                Global.enemyspeed = 4;
                this.node.getComponent(cc.Label).string = "骷髏人";
                this.node.color = cc.color(255,255,255);
                break;

            case "dark_slime" : 
                Global.enemyblood = 200;
                Global.enemyattack = 35;
                Global.enemydefense = 10;
                Global.enemyexp = 5;
                Global.enemygold = 5;
                Global.enemyspeed = 4;
                this.node.getComponent(cc.Label).string = "青頭怪";
                this.node.color = cc.color(255,255,255);
                break;

            case "bone_2" : 
                Global.enemyblood = 150;
                Global.enemyattack = 40;
                Global.enemydefense = 20;
                Global.enemyexp = 8;
                Global.enemygold = 6;
                Global.enemyspeed = 4;
                this.node.getComponent(cc.Label).string = "骷髏士兵";
                this.node.color = cc.color(255,255,255);
                break;
            
            case "wizard_1" : 
                Global.enemyblood = 125;
                Global.enemyattack = 50;
                Global.enemydefense = 25;
                Global.enemyexp = 10;
                Global.enemygold = 7;
                Global.enemyspeed = 6;
                this.node.getComponent(cc.Label).string = "初級法師";
                this.node.color = cc.color(255,255,255);
                break;

            case "bat_2" : 
                Global.enemyblood = 150;
                Global.enemyattack = 65;
                Global.enemydefense = 30;
                Global.enemyexp = 10;
                Global.enemygold = 8;
                Global.enemyspeed = 5;
                this.node.getComponent(cc.Label).string = "大蝙蝠";
                this.node.color = cc.color(255,255,255);
                break;

            case "rock_1" : 
                Global.enemyblood = 450;
                Global.enemyattack = 150;
                Global.enemydefense = 90;
                Global.enemyexp = 22;
                Global.enemygold = 19;
                Global.enemyspeed = 7;
                this.node.getComponent(cc.Label).string = "初級衛兵";
                this.node.color = cc.color(255,255,255);
                break;

            case "bat_3" : 
                Global.enemyblood = 550;
                Global.enemyattack = 160;
                Global.enemydefense = 90;
                Global.enemyexp = 25;
                Global.enemygold = 20;
                Global.enemyspeed = 5;
                this.node.getComponent(cc.Label).string = "紅蝙蝠";
                this.node.color = cc.color(255,255,255);
                break;

            case "knight_1" : 
                Global.enemyblood = 1300;
                Global.enemyattack = 300;
                Global.enemydefense = 150;
                Global.enemyexp = 40;
                Global.enemygold = 35;
                Global.enemyspeed = 4;
                this.node.getComponent(cc.Label).string = "白衣武士";
                this.node.color = cc.color(255,255,255);
                break;

            case "wizard_2" : 
                Global.enemyblood = 500;
                Global.enemyattack = 400;
                Global.enemydefense = 260;
                Global.enemyexp = 47;
                Global.enemygold = 45;
                Global.enemyspeed = 4;
                this.node.getComponent(cc.Label).string = "紅衣法師";
                this.node.color = cc.color(255,255,255);
                break;

            case "bone_3" : 
                Global.enemyblood = 1250;
                Global.enemyattack = 500;
                Global.enemydefense = 400;
                Global.enemyexp = 55;
                Global.enemygold = 55;
                Global.enemyspeed = 5;
                this.node.getComponent(cc.Label).string = "冥衛兵";
                this.node.color = cc.color(255,255,255);
                break;

            case "rock_2" : 
                Global.enemyblood = 1500;
                Global.enemyattack = 560;
                Global.enemydefense = 460;
                Global.enemyexp = 60;
                Global.enemygold = 60;
                Global.enemyspeed = 7;
                this.node.getComponent(cc.Label).string = "高級衛兵";
                this.node.color = cc.color(255,255,255);
                break;

            case "knight_2" : 
                Global.enemyblood = 3000;
                Global.enemyattack = 1700;
                Global.enemydefense = 1500;
                Global.enemyexp = 100;
                Global.enemygold = 100;
                Global.enemyspeed = 4;
                this.node.getComponent(cc.Label).string = "綠衣武士";
                this.node.color = cc.color(255,255,255);
                break;

            case "octopus" : 
                Global.enemyblood = 99999;
                Global.enemyattack = 2000;
                Global.enemydefense = 2000;
                Global.enemyexp = 1000;
                Global.enemygold = 1000;
                this.node.getComponent(cc.Label).string = "血影";
                this.node.color = cc.color(255,255,255);
                break;
        }
    }

}
