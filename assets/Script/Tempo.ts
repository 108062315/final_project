import Global = require("./Global");
import Scene_now = require("./Scene_now");
const {ccclass, property} = cc._decorator;

@ccclass
export default class Tempo extends cc.Component {

    @property({type:cc.AudioClip})
    tempo: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    player_die_sound: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    player_win_sound: cc.AudioClip = null;

    @property([cc.AudioClip])
    skill_sound: cc.AudioClip[] = []; 

    @property(cc.Prefab)
    damage_value: cc.Prefab = null;

    @property([cc.Prefab])
    Beat_Prefabs: cc.Prefab[] = [];

    @property([cc.Prefab])
    Num_prefabs: cc.Prefab[] = [];

    @property([cc.Prefab])
    Input_Prefabs: cc.Prefab[] = [];

    private game_end: boolean = false;

    private command_str: string = "";
    
    private is_in_range: boolean = false;

    private has_input: boolean = false;

    private input_range: number = 0.10;
    private bpm: number = 120;
    private tempo_time: number = 0.5;   

    private music_start: boolean;
    private start_time: number = 0;
    private had_play_tempo: boolean = false;
    private num_beat: number = 0;

    // for key Down
    private SpaceDown: boolean = false;
    private UpDown: boolean = false;
    private DownDown: boolean = false;
    private LeftDown: boolean = false;
    private RightDown: boolean = false;
    
    // skill commands
    private skill_1: string = "r";
    private skill_2: string = "duu";
    private skill_3: string = "lld";
    private skill_4: string = "lddurul";

    private cur_attack:number = Global.playerattack;
    private skill_2_times:number = 0;


    onLoad () {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.tempo_time = 60/this.bpm;
        this.cur_attack = Global.playerattack;
        this.music_start = false;

        if(!Global.has_skill_2){
            cc.find("ability/ability_2/ability_disabled").opacity = 150;
            cc.find("ability/ability_2/name").getComponent(cc.Label).string = "unknown";
            cc.find("ability/ability_2/info").getComponent(cc.Label).string = "???";
            cc.find("ability/ability_2/cmd").getComponent(cc.Label).string = "???";
        }
        if(!Global.has_skill_3){
            cc.find("ability/ability_3/ability_disabled").opacity = 150;
            cc.find("ability/ability_3/name").getComponent(cc.Label).string = "unknown";
            cc.find("ability/ability_3/info").getComponent(cc.Label).string = "???";
            cc.find("ability/ability_3/cmd").getComponent(cc.Label).string = "???";
        }
        if(!Global.has_skill_4){
            cc.find("ability/ability_4/ability_disabled").opacity = 150;
            cc.find("ability/ability_4/name").getComponent(cc.Label).string = "unknown";
            cc.find("ability/ability_4/info").getComponent(cc.Label).string = "???";
            cc.find("ability/ability_4/cmd").getComponent(cc.Label).string = "???";
        }
    }

    start () {
        
    }

    update (dt) {
        //cc.find("Tempo/input_region/Label").getComponent(cc.Label).string = this.command_str;
        if(!this.game_end){
            if(this.music_start == false){    //first time
                //cc.log("Start tempo!!");
                this.music_start = true;

                this.has_input = false;
                this.is_in_range = true;
                this.had_play_tempo = false;
                
                this.scheduleOnce(()=>{cc.audioEngine.playMusic(this.bgm, true);},this.input_range);
                this.start_time = new Date().getTime();
            }
            else{
                let new_time : number = new Date().getTime();
                let cur_time: number = (new_time - this.start_time)/1000- this.num_beat*this.tempo_time;
                //cc.log("cur_time: ", cur_time);
                if(cur_time >= this.tempo_time){
                    this.num_beat ++;
                    
                    this.is_in_range = true;
                    this.had_play_tempo = false;
                }
                else if(cur_time >= this.input_range*2 && this.is_in_range){
                    if(!this.has_input){
                        this.get_command("miss");
                    }
                    this.is_in_range = false;
                    this.has_input = false;
                }
                else if(cur_time >= this.input_range && !this.had_play_tempo){
                    //cc.audioEngine.playEffect(this.tempo, false);
                    this.had_play_tempo = true;
                    this.beat_mark(this.num_beat);
                    if(this.num_beat >15){
                        if((this.num_beat - 15) % Global.enemyspeed == 0){
                            this.enemy_attack();
                        }
                    }
                }
            }
        }

    }
    

    onKeyDown(event)
    {
        switch(event.keyCode)
        {
            case cc.macro.KEY.space:
                if(!this.SpaceDown){
                    if(!this.has_input && this.is_in_range){
                        this.get_command("space");
                    }
                    else{
                        this.get_command("miss");
                    }
                }
                this.SpaceDown = true;
                break;
            case cc.macro.KEY.up:
                if(!this.UpDown){
                    if(!this.has_input && this.is_in_range){
                        this.get_command("up");
                    }
                    else{
                        this.get_command("miss");
                    }
                }
                this.UpDown = true;
                break;
            case cc.macro.KEY.down:
                if(!this.DownDown){
                    if(!this.has_input && this.is_in_range){
                        this.get_command("down");
                    }
                    else{
                        this.get_command("miss");
                    }
                }
                this.DownDown = true;
                break;
            case cc.macro.KEY.left:
                if(!this.LeftDown){
                    if(!this.has_input && this.is_in_range){
                        this.get_command("left");
                    }
                    else{
                        this.get_command("miss");
                    }
                }
                this.LeftDown = true;
                break;
            case cc.macro.KEY.right:
                if(!this.RightDown){
                    if(!this.has_input && this.is_in_range){
                        this.get_command("right");
                    }
                    else{
                        this.get_command("miss");
                    }
                }
                this.RightDown = true;
                break;
        }
    }

    onKeyUp(event)
    {
        switch(event.keyCode)
        {
            case cc.macro.KEY.space:
                this.SpaceDown = false;
                
                break;
            case cc.macro.KEY.up:
                this.UpDown = false;
                
                break;
            case cc.macro.KEY.down:
                this.DownDown = false;
                
                break;
            case cc.macro.KEY.left:
                this.LeftDown = false;
                
                break;
            case cc.macro.KEY.right:
                this.RightDown = false;
                
                break;
        }
    }

    beat_mark(beat: number){
        if(beat >= 8){
            let L_Prefab = cc.instantiate(this.Beat_Prefabs[0]);
            let R_Prefab = cc.instantiate(this.Beat_Prefabs[1]);
            L_Prefab.parent = cc.find("Tempo/prefab");
            R_Prefab.parent = cc.find("Tempo/prefab");
            L_Prefab.position= cc.v2(-400,0);
            R_Prefab.position= cc.v2(400,0);

            let l_finished = cc.callFunc(function(){
                L_Prefab.destroy();
            });
            let r_finished = cc.callFunc(function(){
                R_Prefab.destroy();
            });
            L_Prefab.runAction(cc.fadeIn(1));
            R_Prefab.runAction(cc.fadeIn(1));
            L_Prefab.runAction(cc.sequence(
                cc.moveTo(2, -24.8, 0),
                l_finished)
                );
            R_Prefab.runAction(cc.sequence(
                cc.moveTo(2, 24.8, 0),
                r_finished)
                );
        }
        if(beat == 12){ //3
            let num_Prefab = cc.instantiate(this.Num_prefabs[3]);
            num_Prefab.parent = cc.find("Tempo/prefab");
            num_Prefab.position= cc.v2(0,60);

            let r_finished = cc.callFunc(function(){
                num_Prefab.destroy();
            });
            
            num_Prefab.runAction(cc.fadeOut(0.48));
            num_Prefab.runAction(cc.sequence(
                cc.moveBy(0.48, 0, 25),
                r_finished)
                );
        }
        else if(beat == 13){    //2
            let num_Prefab = cc.instantiate(this.Num_prefabs[2]);
            num_Prefab.parent = cc.find("Tempo/prefab");
            num_Prefab.position= cc.v2(0,60);

            let r_finished = cc.callFunc(function(){
                num_Prefab.destroy();
            });
            
            num_Prefab.runAction(cc.fadeOut(0.48));
            num_Prefab.runAction(cc.sequence(
                cc.moveBy(0.48, 0, 25),
                r_finished)
                );
        }
        else if(beat == 14){    //1
            let num_Prefab = cc.instantiate(this.Num_prefabs[1]);
            num_Prefab.parent = cc.find("Tempo/prefab");
            num_Prefab.position= cc.v2(0,60);

            let r_finished = cc.callFunc(function(){
                num_Prefab.destroy();
            });
            
            num_Prefab.runAction(cc.fadeOut(0.48));
            num_Prefab.runAction(cc.sequence(
                cc.moveBy(0.48, 0, 25),
                r_finished)
                );
        }
        else if(beat == 15){    //Go
            let num_Prefab = cc.instantiate(this.Num_prefabs[0]);
            num_Prefab.parent = cc.find("Tempo/prefab");
            num_Prefab.position= cc.v2(0,60);

            let r_finished = cc.callFunc(function(){
                num_Prefab.destroy();
            });
            
            num_Prefab.runAction(cc.fadeOut(0.48));
            num_Prefab.runAction(cc.sequence(
                cc.moveBy(0.48, 0, 25),
                r_finished)
                );
        }
        
    }

    run_skill(skill: number){
        switch(skill){
            case 1:
                cc.audioEngine.playEffect(this.skill_sound[1], false);
                cc.find("right/enemy/skill_1_3_4").scaleX = 0.2;
                cc.find("right/enemy/skill_1_3_4").scaleY = 0.2;
                cc.find("right/enemy/skill_1_3_4").getComponent(cc.Animation).play("ability1");

                let dmg_Prefab_1 = cc.instantiate(this.damage_value);
                dmg_Prefab_1.parent = cc.find("damage_value");
                dmg_Prefab_1.position = cc.v2(985,532);

                if(this.cur_attack > Global.enemydefense) {
                    dmg_Prefab_1.getComponent(cc.Label).string = "-" + String(Math.floor(this.cur_attack) - Global.enemydefense);

                    Global.enemyblood -= (Math.floor(this.cur_attack) - Global.enemydefense);
                    if(Global.enemyblood<=0){
                        cc.find("right/enemybloodnum").getComponent(cc.Label).string = "生命值:" + 0;
                        this.player_win();
                    }
                    else{
                        cc.find("right/enemybloodnum").getComponent(cc.Label).string = "生命值:" + Global.enemyblood;
                    }
                }
                else{
                    dmg_Prefab_1.getComponent(cc.Label).string = "-0";
                }
        
                let dmg_finished_1 = cc.callFunc(function(){
                    dmg_Prefab_1.destroy();
                });
                
                dmg_Prefab_1.runAction(cc.fadeOut(0.6));
                dmg_Prefab_1.runAction(cc.sequence(
                    cc.moveBy(0.6, -10, 10),
                    dmg_finished_1)
                );


                break;

            case 2:
                cc.audioEngine.playEffect(this.skill_sound[2], false);
                cc.find("left/skill_2").getComponent(cc.Animation).play("ability2");

                this.cur_attack = Math.floor(Global.playerattack*1.5);
                this.skill_2_times ++;
                let cur_times = this.skill_2_times;
                cc.find("left/Playerattacknum/attack_plus").x = cc.find("left/Playerattacknum").width;
                cc.find("left/Playerattacknum/attack_plus").getComponent(cc.Label).string = "(+" + Math.floor(Global.playerattack*0.5) + ")";
                cc.find("left/Playerattacknum").getComponent(cc.Label).string = "攻擊力:" + this.cur_attack;
                this.scheduleOnce(()=>{
                    //cc.log("skill_2_times: cur_times = ", this.skill_2_times, cur_times);
                    if(this.skill_2_times == cur_times){
                        cc.find("left/Playerattacknum/attack_plus").getComponent(cc.Label).string = "";
                        this.cur_attack = Global.playerattack;
                        cc.find("left/Playerattacknum").getComponent(cc.Label).string = "攻擊力:" + Global.playerattack;
                    }
                },10);
                break;

            case 3:
                cc.audioEngine.playEffect(this.skill_sound[3], false);
                cc.find("right/enemy/skill_1_3_4").scaleX = 0.2;
                cc.find("right/enemy/skill_1_3_4").scaleY = 0.2;
                cc.find("right/enemy/skill_1_3_4").getComponent(cc.Animation).play("ability3");

                let dmg_Prefab_3 = cc.instantiate(this.damage_value);
                dmg_Prefab_3.parent = cc.find("damage_value");
                dmg_Prefab_3.position = cc.v2(985,532);
                dmg_Prefab_3.getComponent(cc.Label).string = "-10";

                let dmg_finished_3 = cc.callFunc(function(){
                    dmg_Prefab_3.destroy();
                });
                dmg_Prefab_3.runAction(cc.fadeOut(0.6));
                dmg_Prefab_3.runAction(cc.sequence(
                    cc.moveBy(0.6, -10, 10),
                    dmg_finished_3)
                );

                Global.enemyblood -= 10;
                if(Global.enemyblood<=0){
                    cc.find("right/enemybloodnum").getComponent(cc.Label).string = "生命值:" + 0;
                    this.player_win();
                }
                else{
                    cc.find("right/enemybloodnum").getComponent(cc.Label).string = "生命值:" + Global.enemyblood;
                }
                break;

            case 4:
                cc.audioEngine.playEffect(this.skill_sound[4], false);
                cc.find("right/enemy/skill_1_3_4").scaleX = 0.5;
                cc.find("right/enemy/skill_1_3_4").scaleY = 0.5;
                cc.find("right/enemy/skill_1_3_4").getComponent(cc.Animation).play("ability4");

                let dmg_Prefab_4p = cc.instantiate(this.damage_value);
                dmg_Prefab_4p.parent = cc.find("damage_value");
                dmg_Prefab_4p.position = cc.v2(150,532);

                dmg_Prefab_4p.getComponent(cc.Label).string = "-500";

                let dmg_finished_4p = cc.callFunc(function(){
                    dmg_Prefab_4p.destroy();
                });
                dmg_Prefab_4p.runAction(cc.fadeOut(0.6));
                dmg_Prefab_4p.runAction(cc.sequence(
                    cc.moveBy(0.6, -10, 10),
                    dmg_finished_4p)
                );

                Global.playerblood -= 500;
                if(Global.playerblood <= 0){
                    cc.find("left/Playerbloodnum").getComponent(cc.Label).string = "生命值:" + 0;
                    this.player_die();
                }
                else {

                    let dmg_Prefab_4e = cc.instantiate(this.damage_value);
                    dmg_Prefab_4e.parent = cc.find("damage_value");
                    dmg_Prefab_4e.position = cc.v2(985,532);
                    

                    cc.find("left/Playerbloodnum").getComponent(cc.Label).string = "生命值:" + Global.playerblood;
                    if(this.cur_attack*5 > Global.enemydefense) {
                        dmg_Prefab_4e.getComponent(cc.Label).string = "-" + String(Math.floor(this.cur_attack)*5 - Global.enemydefense);

                        Global.enemyblood -= (Math.floor(this.cur_attack)*5 - Global.enemydefense);
                        if(Global.enemyblood<=0){
                            cc.find("right/enemybloodnum").getComponent(cc.Label).string = "生命值:" + 0;
                            this.player_win();
                        }
                        else {
                            cc.find("right/enemybloodnum").getComponent(cc.Label).string = "生命值:" + Global.enemyblood;
                        }
                        
                    }
                    else{
                        dmg_Prefab_4e.getComponent(cc.Label).string = "-0";
                    }

                    let dmg_finished_4e = cc.callFunc(function(){
                        dmg_Prefab_4e.destroy();
                    });
                    dmg_Prefab_4e.runAction(cc.fadeOut(0.6));
                    dmg_Prefab_4e.runAction(cc.sequence(
                        cc.moveBy(0.6, -10, 10),
                        dmg_finished_4e)
                    );



                }
                
                break;

            case 0: // error skill str
            cc.audioEngine.playEffect(this.skill_sound[0], false);

                let fail_Prefab = cc.instantiate(this.Beat_Prefabs[3]);
                fail_Prefab.parent = cc.find("Tempo/prefab");
                fail_Prefab.position= cc.v2(0,50);

                let fail_finished = cc.callFunc(function(){
                    fail_Prefab.destroy();
                });
                
                fail_Prefab.runAction(cc.fadeOut(0.45));
                fail_Prefab.runAction(cc.sequence(
                    cc.moveBy(0.45, 0, 25),
                    fail_finished)
                    );
                break;

            case -1:    // miss
                let miss_Prefab = cc.instantiate(this.Beat_Prefabs[2]);
                miss_Prefab.parent = cc.find("Tempo/prefab");
                miss_Prefab.position= cc.v2(0,50);

                let miss_finished = cc.callFunc(function(){
                    miss_Prefab.destroy();
                });
                
                miss_Prefab.runAction(cc.fadeOut(0.45));
                miss_Prefab.runAction(cc.sequence(
                    cc.moveBy(0.45, 0, 25),
                    miss_finished)
                    );
                break;
        }
    }


    get_command(cmd: string){
        if(this.game_end) return;
        if(this.num_beat<16) return;
        this.has_input = true;
        switch(cmd){
            case "space":
                if(this.command_str == this.skill_1) this.run_skill(1);
                else if(this.command_str == this.skill_2 && Global.has_skill_2) this.run_skill(2);
                else if(this.command_str == this.skill_3 && Global.has_skill_3) this.run_skill(3);
                else if(this.command_str == this.skill_4 && Global.has_skill_4) this.run_skill(4);
                else this.run_skill(0);
                cc.find("Tempo/input_region").destroyAllChildren();
                this.command_str = "";

                break;
            case "up":
                this.command_str += "u";
                let up_Prefab = cc.instantiate(this.Input_Prefabs[0]);
                up_Prefab.parent = cc.find("Tempo/input_region");
                up_Prefab.runAction(cc.fadeIn(0.2));
                break;
            case "down":
                this.command_str += "d";
                let down_Prefab = cc.instantiate(this.Input_Prefabs[1]);
                down_Prefab.parent = cc.find("Tempo/input_region");
                down_Prefab.runAction(cc.fadeIn(0.2));
                break;
            case "left":
                this.command_str += "l";
                let left_Prefab = cc.instantiate(this.Input_Prefabs[2]);
                left_Prefab.parent = cc.find("Tempo/input_region");
                left_Prefab.runAction(cc.fadeIn(0.2));
                break;
            case "right":
                this.command_str += "r";
                let right_Prefab = cc.instantiate(this.Input_Prefabs[3]);
                right_Prefab.parent = cc.find("Tempo/input_region");
                right_Prefab.runAction(cc.fadeIn(0.2));
                break;
            case "miss":
                this.run_skill(-1);
                cc.find("Tempo/input_region").destroyAllChildren();
                this.command_str = "";
            break;
        }
    }

    enemy_attack(){
        cc.audioEngine.playEffect(this.skill_sound[5], false);
        cc.find("left/Braver/skill_0").getComponent(cc.Animation).play("braverdamaged");

        let dmg_Prefab = cc.instantiate(this.damage_value);
        dmg_Prefab.parent = cc.find("damage_value");
        dmg_Prefab.position = cc.v2(150,532);
        
        if(Global.enemyattack > Global.playerdefense) {

            dmg_Prefab.getComponent(cc.Label).string = "-" + String(Global.enemyattack - Global.playerdefense);

            Global.playerblood -= (Global.enemyattack - Global.playerdefense);
            if(Global.playerblood<=0){
                cc.find("left/Playerbloodnum").getComponent(cc.Label).string = "生命值:" + 0;
                this.player_die();
            }
            else {
                cc.find("left/Playerbloodnum").getComponent(cc.Label).string = "生命值:" + Global.playerblood;
            }
        }
        else{
            dmg_Prefab.getComponent(cc.Label).string = "-0";
        }

        let dmg_finished = cc.callFunc(function(){
            dmg_Prefab.destroy();
        });
        
        dmg_Prefab.runAction(cc.fadeOut(0.6));
        dmg_Prefab.runAction(cc.sequence(
            cc.moveBy(0.6, -10, 10),
            dmg_finished)
        );

    }

    player_win(){
        //cc.log("player win!");
        this.game_end = true;
        Global.playerexp += Global.enemyexp;
        Global.playergold += Global.enemygold;
        if(Global.enemyname == "octopus")
        {
            Scene_now.stage = "victory";
        }


        
        this.scheduleOnce(()=>{

            cc.audioEngine.stopMusic();
            cc.find("Fight_end/bg").runAction(cc.fadeTo(0.5,180));
            this.scheduleOnce(()=>{                
                cc.audioEngine.playEffect(this.player_win_sound, false);
                cc.find("Fight_end/win_title").runAction(cc.fadeTo(0.6,255));
            },0.8);

            this.scheduleOnce(()=>{
                //change scene
                let floor: string = Scene_now.stage;
                cc.director.loadScene(floor);
            },4.5);

        },0.8);
    }

    player_die(){
        //cc.log("player die!");
        this.game_end = true;
        
        this.scheduleOnce(()=>{

            cc.audioEngine.stopMusic();
            cc.find("Fight_end/bg").runAction(cc.fadeTo(0.5,180));
            this.scheduleOnce(()=>{
                cc.audioEngine.playEffect(this.player_die_sound, false);
                cc.find("Fight_end/lose_title").runAction(cc.fadeTo(1,255));
            },0.8);
    
            this.scheduleOnce(()=>{
                //change scene
                cc.director.loadScene("Menu");
            },6.3);

        },0.8);
        
    }

}
