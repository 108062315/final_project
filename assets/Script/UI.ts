import Global = require("./Global");

const {ccclass, property} = cc._decorator;

@ccclass
export default class UI extends cc.Component {

    
    update (dt) {
        this.node.color = cc.color( 255, 255, 255);
        switch (this.node.name) {
            case "UserName" :
                this.node.getComponent(cc.Label).string = Global.Playername;
                break;
            case "Level" :
                this.node.getComponent(cc.Label).string = "" + Global.playerlevel;
                break;
            case "Experience" :
                this.node.getComponent(cc.Label).string = "" + Global.playerexp;
                break;
            case "Blood" :
                this.node.getComponent(cc.Label).string = "" + Global.playerblood;
                break;
            case "Attack" :
                this.node.getComponent(cc.Label).string = "" + Global.playerattack;
                break;
            case "Defense" :
                this.node.getComponent(cc.Label).string = "" + Global.playerdefense;
                break;
            case "CoinNum" :
                this.node.getComponent(cc.Label).string = "" + Global.playergold;
                break;
            case "Key_1Num" :
                    this.node.getComponent(cc.Label).string = "" + Global.playerkey_1;
                break;
            case "Key_2Num" :
                    this.node.getComponent(cc.Label).string = "" + Global.playerkey_2;
                break;
        }
        

    }

    Menu(){
        cc.director.loadScene('Menu');
    }


   
}
