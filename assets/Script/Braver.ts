// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import Scene_now = require("./Scene_now");
import Global = require("./Global");
import stage_info = require("./stage_info");
import { fight_check } from "./Global";

const {ccclass, property} = cc._decorator;

@ccclass
export class Braver extends cc.Component {

    @property(cc.Animation)
    anim: cc.Animation = null;

    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;

    @property(cc.AudioClip)
    change_scene_effect: cc.AudioClip = null;

    @property(cc.AudioClip)
    door_effect: cc.AudioClip = null;

    @property(cc.AudioClip)
    pick_effect: cc.AudioClip = null;

    @property(cc.Prefab)
    new_braver: cc.Prefab = null;

    @property(cc.Node)
    guide1: cc.Node = null;

    @property(cc.Node)
    guide2: cc.Node = null;

    public stopping: number = 0;

    onLoad () {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.director.getPhysicsManager().enabled = true;
        if(Global.floor_check == "off"){
            cc.log("沒上樓啊幹");
            this.node.x = Global.player_pos_x;
            this.node.y = Global.player_pos_y;
        }
    }

    start () {
        // animation
        this.anim = this.node.getComponent(cc.Animation);
        cc.log("Global.player_pos_x: " + Global.player_pos_x);
        cc.log("Global.player_pos_y: " + Global.player_pos_y);
        cc.log("this.node.x: " + this.node.x);
        cc.log("this.node.y: " + this.node.y);
        // 人物面向方向
        if (Global.direction == "up") {
            this.anim.play('Up');
        }else if (Global.direction == "down") {
            this.anim.play('Down');
        }else if (Global.direction == "left") {
            this.anim.play('Left');
        }else if (Global.direction == "right") {
            this.anim.play('Right');
        }

        if (cc.audioEngine.isMusicPlaying() == false) {
            Global.bgmID = cc.audioEngine.playMusic(this.bgm, true);
        }

        // 上樓或下樓

        if (Global.floor_check == "on") {
            Global.floor_check = "off";
            if (Scene_now.stage == '0') {
                if (Global.floor_direction == "up") {
                    this.node.position = cc.v2(368, 48);
                }else {
                    this.node.position = cc.v2(368, 336);
                }
            }else if (Scene_now.stage == '1') {
                if (Global.floor_direction == "up") {
                    this.node.position = cc.v2(368, 80);
                }else {
                    this.node.position = cc.v2(240, 368);
                }
            }else if (Scene_now.stage == '2') {
                if (Global.floor_direction == "up") {
                    this.node.position = cc.v2(208, 336);
                }else {
                    this.node.position = cc.v2(208, 80);
                }
            }else if (Scene_now.stage == '3') {
                if (Global.floor_direction == "up") {
                    this.node.position = cc.v2(240, 48);
                }else {
                    this.node.position = cc.v2(528, 80);
                }
            }else if (Scene_now.stage == '4') {
                if (Global.floor_direction == "up") {
                    this.node.position = cc.v2(528, 80);
                }else {
                    this.node.position = cc.v2(208, 80);
                }
            }else if (Scene_now.stage == '5') {
                if (Global.floor_direction == "up") {
                    this.node.position = cc.v2(240, 48);
                }else {
                    this.node.position = cc.v2(496, 80);
                }
            }else if (Scene_now.stage == '6') {
                if (Global.floor_direction == "up") {
                    this.node.position = cc.v2(240, 48);
                }else {
                    this.node.position = cc.v2(528, 336);
                }
            }else if (Scene_now.stage == '7') {
                if (Global.floor_direction == "up") {
                    this.node.position = cc.v2(240, 368);
                }else {
                    this.node.position = cc.v2(496, 208);
                }
            }else if (Scene_now.stage == '8') {
                if (Global.floor_direction == "up") {
                    this.node.position = cc.v2(272, 336);
                }else {
                    this.node.position = cc.v2(464, 336);
                }
            }else if (Scene_now.stage == '9') {
                if (Global.floor_direction == "up") {
                    this.node.position = cc.v2(432, 368);
                }else {
                    this.node.position = cc.v2(240, 48);
                }
            }else if (Scene_now.stage == '10') {
                if (Global.floor_direction == "up") {
                    this.node.position = cc.v2(368, 80);
                }else {
                    this.node.position = cc.v2(368, 80);
                }
            }
        }
        
        // 戰鬥結束

        if (Global.fight_check == true) {
            Global.fight_check = false;
            this.node.position = cc.v2(Global.player_pos_x, Global.player_pos_y);
            cc.log("finish");
        }

    }

    onKeyDown (event: cc.Event.EventKeyboard) {
        var macro = cc.macro;
        if (event.keyCode == macro.KEY.a || event.keyCode == macro.KEY.left) {
            Global.direction = "left";
            this.node.x -= 32;
            this.anim.play('Left');
        }else if (event.keyCode == macro.KEY.d || event.keyCode == macro.KEY.right) {
            Global.direction = "right";
            this.node.x += 32;
            this.anim.play('Right');
        }else if (event.keyCode == macro.KEY.w || event.keyCode == macro.KEY.up) {
            Global.direction = "up";
            this.node.y += 32;
            this.anim.play('Up');
        }else if (event.keyCode == macro.KEY.s || event.keyCode == macro.KEY.down) {
            Global.direction = "down";
            this.node.y -= 32;
            this.anim.play('Down');
        }else if (event.keyCode == macro.KEY.p) {
            if (Scene_now.stage != '10') {
                cc.audioEngine.playEffect(this.change_scene_effect, false);
                this.change_scene_up();
            }
        }else if (event.keyCode == macro.KEY.o) {
            if (Scene_now.stage != '0') {
                cc.audioEngine.playEffect(this.change_scene_effect, false);
                this.change_scene_down();
            }
        }else if (event.keyCode == macro.KEY.z) {
            if(Global.guide%3 == 0)
            {
                this.guide1.active = true;
                this.guide2.active = false;
            }
            else if(Global.guide%3 == 1)
            {
                this.guide1.active = false;
                this.guide2.active = true;

            }
            else if(Global.guide%3 == 2)
            {
                this.guide1.active = false;
                this.guide2.active = false;
            }
            Global.guide += 1;
        }
    }
    
    onBeginContact(contact, self, other){
        var i = Math.floor((other.node.x - 176) / 32);
        var j = Math.floor(12 - ((other.node.y - 16) / 32));
        var index = (i + 11 * (j - 1) - 1);
        if (other.tag == 1) {             // 上樓
            if (Scene_now.stage != '10') {
                cc.audioEngine.playEffect(this.change_scene_effect, false);
                Global.floor_direction = "up";
                Global.floor_check = "on";
                this.change_scene_up();
            }
        }else if (other.tag == 2) {             // 下樓
            if (Scene_now.stage != '0') {
                cc.audioEngine.playEffect(this.change_scene_effect, false);
                Global.floor_direction = "down";
                Global.floor_check = "on";
                this.change_scene_down();
            }
        }else if (other.node.name == "door_1") {             // door_1
            this.stop();
            if (Global.playerkey_1 > 0 && other.node.getComponent('door').isopening == false) {
                this.record(index);
                other.node.getComponent(cc.Animation).play('door_1');
                other.node.getComponent('door').isopening = true;
                cc.audioEngine.playEffect(this.door_effect, false);
                Global.playerkey_1 -= 1;
                this.scheduleOnce(function () {
                    other.node.active = false;
                }, 0.75);
            }
        }else if (other.node.name == "door_2") {
            this.stop();
            if (Global.playerkey_2 > 0 && other.node.getComponent('door').isopening == false) {
                this.record(index);
                other.node.getComponent(cc.Animation).play('door_2');
                other.node.getComponent('door').isopening = true;
                cc.audioEngine.playEffect(this.door_effect, false);
                Global.playerkey_2 -= 1;
                this.scheduleOnce(function () {
                    other.node.active = false;
                }, 0.75);
            }
        }else if (other.node.name == "key_1") {
            cc.audioEngine.playEffect(this.pick_effect, false);
            other.node.active = false;
            Global.playerkey_1 += 1;
            this.record(index);
        }else if (other.node.name == "key_2") {
            cc.audioEngine.playEffect(this.pick_effect, false);
            other.node.active = false;
            Global.playerkey_2 += 1;
            this.record(index);
        }else if (other.node.name == "red_coin") {
            cc.audioEngine.playEffect(this.pick_effect, false);
            other.node.active = false;
            Global.playerattack += 5;
            this.record(index);
        }else if (other.node.name == "blue_coin") {
            cc.audioEngine.playEffect(this.pick_effect, false);
            other.node.active = false;
            Global.playerdefense += 5;
            this.record(index);
        }else if (other.node.name == "green_coin") {
            cc.audioEngine.playEffect(this.pick_effect, false);
            other.node.active = false;
            Global.playergold += 5;
            this.record(index);
        }else if (other.node.name == "yellow_coin") {
            cc.audioEngine.playEffect(this.pick_effect, false);
            other.node.active = false;
            Global.playergold += 10;
            this.record(index);
        }else if (other.node.name == "red_water") {
            cc.audioEngine.playEffect(this.pick_effect, false);
            other.node.active = false;
            Global.playerblood += 500;
            this.record(index);
        }else if (other.node.name == "blue_water") {
            cc.audioEngine.playEffect(this.pick_effect, false);
            other.node.active = false;
            Global.playerexp += 20;
            this.record(index);
        }else if (other.node.name == "both_water") {
            cc.audioEngine.playEffect(this.pick_effect, false);
            other.node.active = false;
            Global.playerblood += 300;
            Global.playerexp += 20;
            this.record(index);
        }else {
            if (this.stopping == 0) {
                this.stopping = 1;
                this.stop();
                if (other.node.name == "red_slime" || other.node.name == "green_slime" || other.node.name == "dark_slime" || 
                other.node.name == "bat_1" || other.node.name == "bat_2" || other.node.name == "bat_3" || other.node.name == "bat_man" ||
                other.node.name == "bone_1" || other.node.name == "bone_2" || other.node.name == "bone_3" || 
                other.node.name == "wizard_1" || other.node.name == "wizard_2" ||
                other.node.name == "knight_1" || other.node.name == "knight_2" || 
                other.node.name == "rock_1" || other.node.name == "rock_2" ||
                other.node.name == "octopus"
                ) {
                    cc.log("fuck");
                    Global.player_pos_x = this.node.x;
                    Global.player_pos_y = this.node.y;
                    cc.log("Global.player_pos_x: " + Global.player_pos_x);
                    cc.log("Global.player_pos_y: " + Global.player_pos_y);
                    Global.fight_check = true;
                    Global.enemyname = other.node.name;
                    other.node.active = false;
                    this.record(index);
                    cc.director.loadScene('loading');
                }
            }
        }
    }

    stop () {
        // create new one
        var new_braver = cc.instantiate(this.new_braver);
        new_braver.getComponent('Braver').init(this.node);
        this.node.active = false;
    }


    init (node: cc.Node) {
        this.node.parent = cc.find("braver_pre"); // don't mount under the player, otherwise it will change direction when player move
        this.node.position = cc.v2(0, 0);
        this.node.position = this.node.position.addSelf(node.position);
        if (Global.direction == "up") {
            this.node.position = this.node.position.add(cc.v2(0, -32));
        }else if (Global.direction == "down") {
            this.node.position = this.node.position.add(cc.v2(0, 32));
        }else if (Global.direction == "left") {
            this.node.position = this.node.position.add(cc.v2(32, 0));
        }else if (Global.direction == "right") {
            this.node.position = this.node.position.add(cc.v2(-32, 0));
        }
    }

    record (index: number) {
        if (Scene_now.stage == '0') {
            stage_info.stage0[index] = 0;
        }else if (Scene_now.stage == '1') {
            stage_info.stage1[index] = 0;
        }else if (Scene_now.stage == '2') {
            stage_info.stage2[index] = 0;
        }else if (Scene_now.stage == '3') {
            stage_info.stage3[index] = 0;
        }else if (Scene_now.stage == '4') {
            stage_info.stage4[index] = 0;
        }else if (Scene_now.stage == '5') {
            stage_info.stage5[index] = 0;
        }else if (Scene_now.stage == '6') {
            stage_info.stage6[index] = 0;
        }else if (Scene_now.stage == '7') {
            stage_info.stage7[index] = 0;
        }else if (Scene_now.stage == '8') {
            stage_info.stage8[index] = 0;
        }else if (Scene_now.stage == '9') {
            stage_info.stage9[index] = 0;
        }else if (Scene_now.stage == '10') {
            stage_info.stage10[index] = 0;
        }
    }

    change_scene_up () {
        if (Scene_now.stage == '0') {
            Scene_now.stage = "1";
            cc.director.loadScene("1"); 
        }else if (Scene_now.stage == '1') {
            Scene_now.stage = "2";
            cc.director.loadScene("2"); 
        }else if (Scene_now.stage == '2') {
            Scene_now.stage = "3";
            cc.director.loadScene("3"); 
        }else if (Scene_now.stage == '3') {
            Scene_now.stage = "4";
            cc.director.loadScene("4"); 
        }else if (Scene_now.stage == '4') {
            Scene_now.stage = "5";
            cc.director.loadScene("5"); 
        }else if (Scene_now.stage == '5') {
            Scene_now.stage = "6";
            cc.director.loadScene("6"); 
        }else if (Scene_now.stage == '6') {
            Scene_now.stage = "7";
            cc.director.loadScene("7"); 
        }else if (Scene_now.stage == '7') {
            Scene_now.stage = "8";
            cc.director.loadScene("8"); 
        }else if (Scene_now.stage == '8') {
            Scene_now.stage = "9";
            cc.director.loadScene("9"); 
        }else if (Scene_now.stage == '9') {
            Scene_now.stage = "10";
            cc.director.loadScene("10"); 
        }
    }

    change_scene_down () {
        if (Scene_now.stage == '1') {
            Scene_now.stage = "0";
            cc.director.loadScene("0"); 
        }else if (Scene_now.stage == '2') {
            Scene_now.stage = "1";
            cc.director.loadScene("1"); 
        }else if (Scene_now.stage == '3') {
            Scene_now.stage = "2";
            cc.director.loadScene("2"); 
        }else if (Scene_now.stage == '4') {
            Scene_now.stage = "3";
            cc.director.loadScene("3"); 
        }else if (Scene_now.stage == '5') {
            Scene_now.stage = "4";
            cc.director.loadScene("4"); 
        }else if (Scene_now.stage == '6') {
            Scene_now.stage = "5";
            cc.director.loadScene("5"); 
        }else if (Scene_now.stage == '7') {
            Scene_now.stage = "6";
            cc.director.loadScene("6"); 
        }else if (Scene_now.stage == '8') {
            Scene_now.stage = "7";
            cc.director.loadScene("7"); 
        }else if (Scene_now.stage == '9') {
            Scene_now.stage = "8";
            cc.director.loadScene("8"); 
        }else if (Scene_now.stage == '10') {
            Scene_now.stage = "9";
            cc.director.loadScene("9"); 
        }
    }
    
}
