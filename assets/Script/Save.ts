const {ccclass, property} = cc._decorator;

import Global = require("./Global");
import stage_info = require("./stage_info");
import Scene_now = require("./Scene_now");

@ccclass
export default class Save extends cc.Component {

    save(){
        cc.log("name: ", Global.Playername);
        
        Global.is_new = false;
        Global.player_pos_x = cc.find("Braver").x;
        Global.player_pos_y = cc.find("Braver").y;
        cc.log("save");
        alert("Save success!");
        var Ref = firebase.database().ref(Global.Playername);
        var data = {
            Is_new: false,
            Email: Global.email,
            fb_Global: Global,
            fb_stage_info: stage_info,
            fb_scene_now: Scene_now.stage
        };
        Ref.set(data);
        cc.log("is_new", Global.is_new);
    }
}
