const {ccclass, property} = cc._decorator;

var interval

@ccclass
export default class Player extends cc.Component {

    @property(cc.Node)
    Main_Camera: cc.Node = null;
    


    onLoad () {
        this.node.scaleX = 100;
        this.node.scaleY = 100;
    }

    start () {
        interval = window.setInterval(( () => 
        {
            this.node.scaleX = this.node.scaleX * 0.94;
            this.node.scaleY = this.node.scaleY * 0.94;
        }            
        ), 15);
    }
 
    update(dt) {
        if(this.node.scaleX <= 0.4)
        {
            window.clearInterval(interval);
        }
    }

}