const {ccclass, property} = cc._decorator;

import Global = require("./Global");

@ccclass
export default class Load extends cc.Component {

    load(){
        cc.log("load");
        alert("Load!");
        var Ref = firebase.database().ref(Global.Playername);
        Ref.once('value')
            .then(function(snapshot) {
                var data = snapshot.val();
                cc.log(data);
                
                Global.playerattack = data.attack;
                Global.playerblood = data.blood;
                Global.playerdefense = data.defense;
                Global.playerlevel = data.level;
                Global.playerexp = data.experience;
                Global.playergold = data.coin;
                Global.playerkey_1 = data.key_1;
                Global.playerkey_2 = data.key_2;  
                            
            })
            .catch(e => console.log(e.message));   
    }
}
