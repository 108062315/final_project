
const {ccclass, property} = cc._decorator;

import Global = require("./Global");

@ccclass
export default class NewClass extends cc.Component {
    
    @property(cc.Sprite)
    dialogue: cc.Sprite = null;
    @property(cc.Sprite)
    frame: cc.Sprite = null;
    @property(cc.Sprite)
    roleIcon: cc.Sprite = null;
    

    @property(cc.Label)
    nameLabel: cc.Label = null;
    @property(cc.Label)
    textLabel: cc.Label = null;
    @property(cc.Button)
    Attack: cc.Button = null;
    @property(cc.Label)
    AttackLabel: cc.Label = null;

    @property(cc.Button)
    Defense: cc.Button = null;
    @property(cc.Label)
    DefenseLabel: cc.Label = null;
    @property(cc.Button)
    closeIcon: cc.Button = null;

    @property(cc.Sprite)
    closeBackground: cc.Sprite = null;
    @property(cc.Sprite)
    attackBackground: cc.Sprite = null;
    @property(cc.Sprite)
    defenseBackground: cc.Sprite = null;

    dialogue_show : boolean = false;
    // LIFE-CYCLE CALLBACKS:

    nameString : string = "商店";
    text : string = "25個金幣可以兌換:";
    a : string = "攻擊+5";
    d : string = "防禦+5";

    onLoad () {
        this.dialogue.node.height = 0;
        this.frame.node.height = 0;
        this.roleIcon.node.height = 0;
        this.closeIcon.node.height = 0;
        this.nameLabel.node.height = 0;
        this.textLabel.node.height = 0;
        this.Attack.node.height = 0;
        this.AttackLabel.node.height = 0;
        this.Defense.node.height = 0;
        this.DefenseLabel.node.height = 0;

        this.closeBackground.node.height = 0;
        this.attackBackground.node.height = 0;
        this.defenseBackground.node.height = 0;

        this.dialogue_show = false;
        
    }

    update(dt){
        
    }

    Attack_Up(){
        if(Global.playergold >= 25){
            Global.playerattack = Global.playerattack + 5;
            Global.playergold = Global.playergold - 25;
        }
        else{
            alert("YOU DON'T HAVE ENOUGH GOLD");
        }
        
    }

    Defense_Up(){
        if(Global.playergold >= 25){
            Global.playerdefense = Global.playerdefense + 5;
            Global.playergold = Global.playergold - 25;
        }
        else{
            alert("YOU DON'T HAVE ENOUGH GOLD");
        }
        
    }

    close_window(){
        this.nameLabel.node.getComponent(cc.Label).string = "";
        this.textLabel.node.getComponent(cc.Label).string = "";
        this.AttackLabel.node.getComponent(cc.Label).string = "";
        this.DefenseLabel.node.getComponent(cc.Label).string = "";

        this.dialogue.node.height = 0;
        this.frame.node.height = 0;
        this.roleIcon.node.height = 0;
        this.closeIcon.node.height = 0;
        
        this.nameLabel.node.height = 0;
        this.textLabel.node.height = 0;
        this.Attack.node.height = 0;
        this.AttackLabel.node.height = 0;
        this.Defense.node.height = 0;
        this.DefenseLabel.node.height = 0;

        this.closeBackground.node.height = 0;
        this.attackBackground.node.height = 0;
        this.defenseBackground.node.height = 0;
        

        this.dialogue_show = false;
    }
    onBeginContact(contact, self, other){
        //cc.log(other.node.name);
        if(other.node.name == "Braver"){
            //alert(other.node.name);
            
            this.nameLabel.node.getComponent(cc.Label).string = this.nameString;
            this.textLabel.node.getComponent(cc.Label).string = this.text;
            this.AttackLabel.node.getComponent(cc.Label).string = this.a;
            this.DefenseLabel.node.getComponent(cc.Label).string = this.d;

            this.dialogue.node.height = 150;
            this.frame.node.height = 64;
            this.roleIcon.node.height = 50;
            this.closeIcon.node.height = 32;

            this.closeBackground.node.height = 30;
            this.attackBackground.node.height = 30;
            this.defenseBackground.node.height = 30;

            this.nameLabel.node.height = 25;
            this.textLabel.node.height = 50;
            this.Attack.node.height = 30;
            this.AttackLabel.node.height = 30;
            this.Defense.node.height = 30;
            this.DefenseLabel.node.height = 30;

            this.dialogue_show = true;
        }
    }
}
