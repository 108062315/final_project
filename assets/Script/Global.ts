var Global = {
    is_new: false,
    
    email: "",
    Playername: "NO00",

    playerblood:1000,
    playerattack:10,
    playerdefense:10,

    playerexp: 0,
    playergold: 0,
    
    playerlevel:1,
    playerkey_1:0,
    playerkey_2:0,
    
    enemyname:"bat_1",
    enemyblood:9999,
    enemyattack:999,
    enemydefense:99,
    enemyexp:10,
    enemygold:10,
    enemyspeed:5,

    player_pos_x:368,
    player_pos_y:48,
    
    direction: "down",
    floor_direction: "up",
    floor_check: "off",

    fight_check: false,
    bgmID:0,
    backgroundvolume:1,
    effectvolume:1,

    guide: 0,

    has_skill_2: false,
    has_skill_3: false,
    has_skill_4: false

};

export = Global;
