import Global = require("./Global");

const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends cc.Component {

    @property(cc.Node)
    Main_Camera: cc.Node = null;
    

    @property({type:cc.Animation})
    animator: cc.Animation = null;

    onLoad () {
        
        this.node.color = cc.color(0,0,0);
        switch (this.node.name) {
            case "Playername":
                this.node.getComponent(cc.Label).string = Global.Playername;
                this.node.color = cc.color(255,255,255);
                break;
            case "Playerbloodnum":
                this.node.getComponent(cc.Label).string = "生命值:" + Global.playerblood;
                break;
            case "Playerattacknum":
                this.node.getComponent(cc.Label).string = "攻擊力:" + Global.playerattack;
                break;
            case "Playerdefensenum":
                this.node.getComponent(cc.Label).string = "防禦力:" + Global.playerdefense;
                break;
            case "enemybloodnum":
                this.node.getComponent(cc.Label).string = "生命值:" + Global.enemyblood;
                break;
            case "enemyattacknum":
                this.node.getComponent(cc.Label).string ="攻擊力:" + Global.enemyattack;
                break;
            case "enemydefensenum":
                this.node.getComponent(cc.Label).string = "防禦力:" + Global.enemydefense;
                break;
            case "enemyname":
                this.node.color = cc.color(255,255,255);
                break;
        }
    }

}
