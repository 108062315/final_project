const {ccclass, property} = cc._decorator;

import Global = require("./Global");
import stage_info = require("./stage_info");
import Scene_now = require("./Scene_now");


@ccclass
export default class Player extends cc.Component {

    @property(cc.Node)
    lgaccount: cc.Node = null;

    @property(cc.Node)
    lgpassword: cc.Node = null;

    @property(cc.Node)
    suusername: cc.Node = null;

    @property(cc.Node)
    suaccount: cc.Node = null;

    @property(cc.Node)
    supassword: cc.Node = null;

    


    @property(cc.AudioClip)
    bgmmm: cc.AudioClip = null;

    @property(cc.Node)
    logininfo: cc.Node = null;

    @property(cc.Node)
    signupinfo: cc.Node = null;
    


    start() {
        if(this.node.name == "title")
        {
            this.scheduleOnce(()=>{                
                this.node.runAction(cc.fadeTo(5,255));
            },0.2);
            cc.audioEngine.playEffect(this.bgmmm, false);
        }
        else if(this.node.name == "lg")
        {
            this.scheduleOnce(()=>{                
                this.node.runAction(cc.fadeTo(4,255));
            },6);
        }

        else if(this.node.name == "su")
        {
            this.scheduleOnce(()=>{                
                this.node.runAction(cc.fadeTo(4,255));
            },9);
        }
        else if(this.node.name == "cover")
        {
            this.scheduleOnce(()=>{                
                this.node.active = false;
            },5.2);
        }
    }

    loginin() {
        this.logininfo.active = true;
        this.signupinfo.active = false;
    }

    signinin () {
        this.signupinfo.active = true;
        this.logininfo.active = false;
    }

    login(){

        var txtEmail = this.lgaccount.getComponent(cc.Label).string;
        var txtPassword = this.lgpassword.getComponent(cc.Label).string;
        
        firebase.auth().signInWithEmailAndPassword(txtEmail,txtPassword).then(function() {
            Global.email = txtEmail;
            
            cc.director.loadScene("Menu");
        }).catch(function(error) {
            alert(error.message);
        });

    }

    signup(){
        var txtEmail = this.suaccount.getComponent(cc.Label).string;
        var txtPassword = this.supassword.getComponent(cc.Label).string;
        var txtusername = this.suusername.getComponent(cc.Label).string;
        
        if(txtusername.indexOf(".") != -1 || txtusername.indexOf("#") != -1 ||txtusername.indexOf("$") != -1 || txtusername.indexOf("[") != -1 || txtusername.indexOf("]") != -1){
            alert("User name shouldn't contain '.' or '#' or '$' or '[' or ']'");
            return;
        }
        else{
            var user_ref = firebase.database().ref("/user_data");
            user_ref.once('value', function (snapshot) {
                if (snapshot.hasChild(txtusername)) {
                    alert("This user name has been used, plz change another!");
                    return;
                }
                else{
                    
                    firebase.auth().createUserWithEmailAndPassword(txtEmail, txtPassword).then(function(result) {
                        alert("Sign Up success!");
                        Global.is_new = true;
                        Global.Playername = txtusername;
                        Global.email = txtEmail;
                        var Ref = firebase.database().ref(txtusername);
                        var data = {
                            Is_new: true,
                            Email: txtEmail,
                            fb_Global: Global,
                            fb_stage_info: stage_info,
                            fb_scene_now: Scene_now.stage
                        };
                        Ref.set(data);
                        firebase.auth().signInWithEmailAndPassword(txtEmail,txtPassword).then(function() {
                            alert('Auto Login...');
                            cc.director.loadScene("Menu");
                        }).catch(function(error) {
                            alert(error.message);
                        });
        
                    }).catch(function(error) {
                        txtEmail = "";
                        txtPassword = "";
                        alert(error);
                    });
                }
            });
        }

        Global.email = this.suaccount.getComponent(cc.Label).string;
        Global.Playername = this.suusername.getComponent(cc.Label).string;
        

    }

}