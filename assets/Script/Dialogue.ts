
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    @property(cc.Sprite)
    dialogue: cc.Sprite = null;
    @property(cc.Sprite)
    roleIcon: cc.Sprite = null;
    @property(cc.Label)
    nameLabel: cc.Label = null;
    @property(cc.Label)
    textLabel: cc.Label = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        
    }

    start () {

    }

    // update (dt) {}

    onBeginContact(contact, self, other){
        //cc.log(other.node.name);
        if(other.node.name == "Braver"){
            alert(other.node.name);

        }
    }
}
