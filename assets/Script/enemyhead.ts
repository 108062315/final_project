import Global = require("./Global");



const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends cc.Component {

    @property(cc.Node)
    Main_Camera: cc.Node = null;
    

    @property({type:cc.Animation})
    animator: cc.Animation = null;



    

    onLoad () {
        this.animator = this.getComponent(cc.Animation);
        switch (Global.enemyname) {
            case "bat_1":
                this.animator.play("bat_1");
                break;
            case "bat_2":
                this.animator.play("bat_2");
                break;
            case "bat_3":
                this.animator.play("bat_3");
                break;
            case "bat_man":
                this.animator.play("bat_man");
                break;
            case "bone_1":
                this.animator.play("bone_1");
                break;
            case "bone_2":
                this.animator.play("bone_2");
                break;
            case "bone_3":
                this.animator.play("bone_3");
                break;
            case "dark_slime":
                this.animator.play("dark_slime");
                break;
            case "green_slime":
                this.animator.play("green_slime");
                break;
            case "knight_1":
                this.animator.play("knight_1");
                break;
            case "knight_2":
                this.animator.play("knight_2");
                break;
            case "octopus":
                this.animator.play("octopus");
                if(this.node.name == "enemyhead")
                {
                    this.node.scaleX = 1;
                    this.node.scaleY = 1;
                }
                else if(this.node.name == "enemy")
                {
                    this.node.scaleX = 2.5;
                    this.node.scaleY = 2.5;   
                }
                break;
            case "red_slime":
                this.animator.play("red_slime");
                break;
            case "rock_1":
                this.animator.play("rock_1");
                break;
            case "rock_2":
                this.animator.play("rock_2");
                break;
            case "wizard_1":
                this.animator.play("wizard_1");
                break;
            case "wizard_2":
                this.animator.play("wizard_2");
                break;



        }
    }

    start () {
        if(this.node.scaleX == 2 || this.node.scaleX == 1)
        {
            setTimeout(() => {
                this.animator.pause();
            }, 1);
        }
    }

    update (dt) {
        
    }

}
