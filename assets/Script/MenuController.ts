import Global = require("./Global");
const {ccclass, property} = cc._decorator;

@ccclass
export default class MenuController extends cc.Component {

    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;

    @property(cc.AudioClip)
    button_effect: cc.AudioClip = null;

    @property(cc.Slider)
    slider_1: cc.Slider = null;

    @property(cc.Slider)
    slider_2: cc.Slider = null;

    
    onLoad () {

        if(this.node.name == "Handle")
        {
            this.node.setPosition(Math.floor(-150+Global.backgroundvolume*300),this.node.y);
        }
        else if(this.node.name == "Handle2")
        {
            this.node.setPosition(Math.floor(-150+Global.effectvolume*300),this.node.y);
        }
    }

    back () {
        cc.audioEngine.playEffect(this.button_effect, false);
        cc.director.loadScene('Menu');
    }

    Slider_1 () {
        cc.audioEngine.setVolume(Global.bgmID, this.slider_1.progress);
        Global.backgroundvolume = this.slider_1.progress;
    }

    Slider_2 () {
        cc.audioEngine.setEffectsVolume(this.slider_2.progress);
        Global.effectvolume = this.slider_2.progress;
    }

}
