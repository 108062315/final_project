import Global = require("./Global");
const {ccclass, property} = cc._decorator;

@ccclass
export default class Tempo extends cc.Component {

    @property({type:cc.AudioClip})
    tempo: cc.AudioClip = null;

    @property({type:cc.AudioClip})
    bgm: cc.AudioClip = null;

    @property(cc.Prefab)
    Prefabs: cc.Prefab = null;

    private command_str: string = "";
    
    private is_in_range: boolean = false;

    private has_input: boolean = false;

    private input_range: number = 0.10;
    private bpm: number = 120;
    private tempo_time: number = 0.5;    //0.4854

    private time_flag: boolean;
    private dt_Timer: number = 0
    private last_time: number = 0;
    private last_delay: number = 0;
    private had_play_tempo: boolean = false;


    // for key Down
    private SpaceDown: boolean = false;
    private UpDown: boolean = false;
    private DownDown: boolean = false;
    private LeftDown: boolean = false;
    private RightDown: boolean = false;
    
    // skill commands
    private skill_1: string = "r";
    private skill_2: string = "uuu";
    private skill_3: string = "udlr";
    private skill_4: string = "udlrudlr";


    onLoad () {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.tempo_time = 60/this.bpm;
        /*this.Tempo = ()=> {
            this.scheduleOnce(()=>{
                
            }, this.input_range);
            
            this.has_input = false;
            this.is_in_range = true;
            this.scheduleOnce(()=>{
                this.is_in_range = false;
                if(!this.has_input){
                    this.get_command("miss");
                }
            }, this.input_range*2);
        }*/
        this.time_flag = false;
        this.dt_Timer = 0;
        this.has_input = false;
        this.had_play_tempo = false;
        this.last_delay = 0;
    }

    start () {
        /*this.schedule(()=>{
            cc.audioEngine.playEffect(this.tempo, false);
        }, this.tempo_time, cc.macro.REPEAT_FOREVER, this.input_range);*/
        
        
        this.schedule(()=>{
            let L_Prefab = cc.instantiate(this.Prefabs);
            let R_Prefab = cc.instantiate(this.Prefabs);
            L_Prefab.parent = cc.find("Tempo/prefab");
            R_Prefab.parent = cc.find("Tempo/prefab");
            L_Prefab.position= cc.v2(-530,0);
            R_Prefab.position= cc.v2(525,0);
            let finished = cc.callFunc(function(){
                L_Prefab.destroy();
                R_Prefab.destroy();
            });

            
            L_Prefab.runAction(cc.sequence(
                cc.moveTo(2, 0, 0),
                finished)
                );
            R_Prefab.runAction(cc.sequence(
                cc.moveTo(2, 0, 0),
                finished)
                );

        },this.tempo_time, cc.macro.REPEAT_FOREVER, 0);
        

    }

    update (dt) {
        cc.find("Tempo/input_region/Label").getComponent(cc.Label).string = this.command_str;
        if(this.time_flag == false){    //first time
            cc.log("Start tempo!!")
            this.time_flag = true;
            this.dt_Timer = dt;

            this.is_in_range = true;
            
            this.scheduleOnce(()=>{cc.audioEngine.playMusic(this.bgm, true);},this.input_range);
            this.last_time = new Date().getTime();
        }
        else{
            if(this.dt_Timer == 0){
                this.has_input = false;
                this.is_in_range = true;
                this.had_play_tempo = false;

                let new_time = new Date().getTime();
                let delay = (new_time - this.last_time)/1000 - this.tempo_time - this.last_delay;

                this.last_delay = delay;
                this.last_time = new_time;
            }
            else if(this.dt_Timer >= (this.input_range + this.last_delay - dt) && !this.had_play_tempo){
                cc.audioEngine.playEffect(this.tempo, false);
                this.had_play_tempo = true;

            }
            else if(this.dt_Timer >= (this.input_range*2 + this.last_delay - dt)){
                this.is_in_range = false;
                if(!this.has_input){
                    this.get_command("miss");
                }
            }
            this.dt_Timer += dt;
        }

        
        if(this.dt_Timer >= (this.tempo_time + this.last_delay-dt) ){
            this.dt_Timer = 0;
        }

        

        /*if(this.time_flag){
            this.has_input = false;
            this.is_in_range = true;

            this.time_flag = false;
            let new_time = new Date().getTime();
            

            let delay = (new_time - this.last_time)/1000 - this.tempo_time - this.last_delay;
            
            cc.log(delay);
            if(delay < dt && delay > -dt){
                this.dt_Timer += delay;
                this.last_delay = delay;
            }
            else{
                this.last_delay = 0;
            }

            this.last_time = new_time;
        }
        else if(this.dt_Timer >= this.input_range && !this.had_play_tempo){
            cc.audioEngine.playEffect(this.tempo, false);
            this.had_play_tempo = true;
        }
        else if(this.dt_Timer >= this.input_range*2){
            this.is_in_range = false;
            if(!this.has_input){
                this.get_command("miss");
            }
        }

        this.dt_Timer += dt;

        if(this.dt_Timer >= this.tempo_time-0.5*dt){
            this.dt_Timer = 0;
            this.time_flag = true;
            this.had_play_tempo = false;
        }*/
    }
    

    onKeyDown(event)
    {
        switch(event.keyCode)
        {
            case cc.macro.KEY.space:
                if(!this.has_input && !this.SpaceDown){
                    if(this.is_in_range){
                        this.get_command("space");
                    }
                    else{
                        this.get_command("miss");
                    }
                }
                this.SpaceDown = true;
                break;
            case cc.macro.KEY.up:
                if(!this.has_input && !this.UpDown){
                    if(this.is_in_range){
                        this.get_command("up");
                    }
                    else{
                        this.get_command("miss");
                    }
                }
                this.UpDown = true;
                break;
            case cc.macro.KEY.down:
                if(!this.has_input && !this.DownDown){
                    if(this.is_in_range){
                        this.get_command("down");
                    }
                    else{
                        this.get_command("miss");
                    }
                }
                this.DownDown = true;
                break;
            case cc.macro.KEY.left:
                if(!this.has_input && !this.LeftDown){
                    if(this.is_in_range){
                        this.get_command("left");
                    }
                    else{
                        this.get_command("miss");
                    }
                }
                this.LeftDown = true;
                break;
            case cc.macro.KEY.right:
                if(!this.has_input && !this.RightDown){
                    if(this.is_in_range){
                        this.get_command("right");
                    }
                    else{
                        this.get_command("miss");
                    }
                }
                this.RightDown = true;
                break;
        }
    }

    onKeyUp(event)
    {
        switch(event.keyCode)
        {
            case cc.macro.KEY.space:
                this.SpaceDown = false;
                
                break;
            case cc.macro.KEY.up:
                this.UpDown = false;
                
                break;
            case cc.macro.KEY.down:
                this.DownDown = false;
                
                break;
            case cc.macro.KEY.left:
                this.LeftDown = false;
                
                break;
            case cc.macro.KEY.right:
                this.RightDown = false;
                
                break;
        }
    }

    run_skill(skill: number){
        switch(skill){
            case 1:

                break;
            case 2:
                 
                break;
            case 3:
                 
                break;
            case 4:
                 
                break;
            case 0: // error skill str
                 
                break;
        }
    }


    get_command(cmd: string){
        this.has_input = true;
        switch(cmd){
            case "space":
                if(this.command_str == this.skill_1) this.run_skill(1);
                else if(this.command_str == this.skill_2) this.run_skill(2);
                else if(this.command_str == this.skill_3) this.run_skill(3);
                else if(this.command_str == this.skill_4) this.run_skill(4);
                else this.run_skill(0);

                this.command_str = "";

                break;
            case "up":
                this.command_str += "u";
                break;
            case "down":
                
                this.command_str += "d";
                break;
            case "left":
                
                this.command_str += "l";
                break;
            case "right":
                
                this.command_str += "r";
                break;
            case "miss":
                this.command_str = "";
            break;
        }
    }



}
