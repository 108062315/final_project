// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import stage_info = require("./stage_info");
import Scene_now = require("./Scene_now");

const {ccclass, property} = cc._decorator;

@ccclass
export default class enemycheck extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';


    start () {
        var i = Math.floor((this.node.x - 176) / 32);
        var j = Math.floor(12 - ((this.node.y - 16) / 32));
        var index = (i + 11 * (j - 1) - 1);
        this.record(index);
    }

    record (index: number) {
        if (Scene_now.stage == '0') {
            if (stage_info.stage0[index] == 0) {
                this.node.active = false;
            }
        }else if (Scene_now.stage == '1') {
            if (stage_info.stage1[index] == 0) {
                this.node.active = false;
            }
        }else if (Scene_now.stage == '2') {
            if (stage_info.stage2[index] == 0) {
                this.node.active = false;
            }
        }else if (Scene_now.stage == '3') {
            if (stage_info.stage3[index] == 0) {
                this.node.active = false;
            }
        }else if (Scene_now.stage == '4') {
            if (stage_info.stage4[index] == 0) {
                this.node.active = false;
            }
        }else if (Scene_now.stage == '5') {
            if (stage_info.stage5[index] == 0) {
                this.node.active = false;
            }
        }else if (Scene_now.stage == '6') {
            if (stage_info.stage6[index] == 0) {
                this.node.active = false;
            }
        }else if (Scene_now.stage == '7') {
            if (stage_info.stage7[index] == 0) {
                this.node.active = false;
            }
        }else if (Scene_now.stage == '8') {
            if (stage_info.stage8[index] == 0) {
                this.node.active = false;
            }
        }else if (Scene_now.stage == '9') {
            if (stage_info.stage9[index] == 0) {
                this.node.active = false;
            }
        }else if (Scene_now.stage == '10') {
            if (stage_info.stage10[index] == 0) {
                this.node.active = false;
            }
        }
    }

}


