
const {ccclass, property} = cc._decorator;

import Global = require("./Global");

@ccclass
export default class NewClass extends cc.Component {
    
    @property(cc.Sprite)
    dialogue: cc.Sprite = null;
    @property(cc.Sprite)
    frame: cc.Sprite = null;
    @property(cc.Sprite)
    roleIcon: cc.Sprite = null;
    

    @property(cc.Label)
    nameLabel: cc.Label = null;
    @property(cc.Label)
    textLabel: cc.Label = null;
    @property(cc.Button)
    Attack: cc.Button = null;
    @property(cc.Label)
    AttackLabel: cc.Label = null;

    @property(cc.Button)
    Defense: cc.Button = null;
    @property(cc.Label)
    DefenseLabel: cc.Label = null;
    @property(cc.Button)
    closeIcon: cc.Button = null;

    @property(cc.Sprite)
    closeBackground: cc.Sprite = null;
    @property(cc.Sprite)
    Key1Background: cc.Sprite = null;
    @property(cc.Sprite)
    Key2Background: cc.Sprite = null;

    dialogue_show : boolean = false;
    // LIFE-CYCLE CALLBACKS:

    nameString : string = "商人";
    text : string = "黃鑰匙需25金幣；綠鑰匙需50金幣";
    a : string = "黃鑰匙";
    d : string = "綠鑰匙";

    onLoad () {
        this.dialogue.node.height = 0;
        this.frame.node.height = 0;
        this.roleIcon.node.height = 0;
        this.closeIcon.node.height = 0;
        this.nameLabel.node.height = 0;
        this.textLabel.node.height = 0;
        this.Attack.node.height = 0;
        this.AttackLabel.node.height = 0;
        this.Defense.node.height = 0;
        this.DefenseLabel.node.height = 0;

        this.closeBackground.node.height = 0;
        this.Key1Background.node.height = 0;
        this.Key2Background.node.height = 0;

        this.dialogue_show = false;
        
    }

    update(dt){
        
    }

    Attack_Up(){
        if(Global.playergold >= 25){
            Global.playerkey_1 = Global.playerkey_1 + 1;
            Global.playergold = Global.playergold - 25;
        }
        else{
            alert("YOU DON'T HAVE ENOUGH GOLD");
        }
        
    }

    Defense_Up(){
        if(Global.playergold >= 25){
            Global.playerkey_2 = Global.playerkey_2 + 1;
            Global.playergold = Global.playergold - 25;
        }
        else{
            alert("YOU DON'T HAVE ENOUGH GOLD");
        }
        
    }

    close_window(){
        this.nameLabel.node.getComponent(cc.Label).string = "";
        this.textLabel.node.getComponent(cc.Label).string = "";
        this.AttackLabel.node.getComponent(cc.Label).string = "";
        this.DefenseLabel.node.getComponent(cc.Label).string = "";

        this.dialogue.node.height = 0;
        this.frame.node.height = 0;
        this.roleIcon.node.height = 0;
        this.closeIcon.node.height = 0;

        this.closeBackground.node.height = 0;
        this.Key1Background.node.height = 0;
        this.Key2Background.node.height = 0;
        
        this.nameLabel.node.height = 0;
        this.textLabel.node.height = 0;
        this.Attack.node.height = 0;
        this.AttackLabel.node.height = 0;
        this.Defense.node.height = 0;
        this.DefenseLabel.node.height = 0;

        this.dialogue_show = false;
    }
    onBeginContact(contact, self, other){
        //cc.log(other.node.name);
        if(other.node.name == "Braver"){
            //alert(other.node.name);
            
            this.nameLabel.node.getComponent(cc.Label).string = this.nameString;
            this.textLabel.node.getComponent(cc.Label).string = this.text;
            this.AttackLabel.node.getComponent(cc.Label).string = this.a;
            this.DefenseLabel.node.getComponent(cc.Label).string = this.d;

            this.dialogue.node.height = 150;
            this.frame.node.height = 64;
            this.roleIcon.node.height = 50;
            this.closeIcon.node.height = 32;
            
            this.closeBackground.node.height = 32;
            this.Key1Background.node.height = 30;
            this.Key2Background.node.height = 30;
            

            this.nameLabel.node.height = 25;
            this.textLabel.node.height = 40;
            this.Attack.node.height = 30;
            this.AttackLabel.node.height = 30;
            this.Defense.node.height = 30;
            this.DefenseLabel.node.height = 30;
            
            this.dialogue_show = true;
        }
    }
}
