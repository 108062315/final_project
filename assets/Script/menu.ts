const {ccclass, property} = cc._decorator;

import Global = require("./Global");
import stage_info = require("./stage_info");
import Scene_now = require("./Scene_now");

@ccclass
export default class menu extends cc.Component {

    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;


    play () {   //new game
        
        Global.playerblood = 1000;
        Global.playerattack = 10;
        Global.playerdefense = 10;
        Global.playerexp = 0;
        Global.playergold = 0;
        Global.playerlevel = 1;
        Global.playerkey_1 = 0;
        Global.playerkey_2 = 0;

        Global.player_pos_x = 368;
        Global.player_pos_y = 48;

        Global.direction = "down";
        Global.floor_direction = "up";
        Global.floor_check = "off";

        Global.has_skill_2 = false;
        Global.has_skill_3 = false;
        Global.has_skill_4 = false;
        Scene_now.stage = "0";
        stage_info.stage0 = 
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        stage_info.stage1 = 
           [0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 1, 2, 0, 0, 2, 2, 2, 0, 0,
            2, 0, 2, 0, 0, 0, 2, 2, 2, 0, 0,
            0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            0, 1, 0, 0, 0, 2, 1, 1, 0, 0, 1,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 1, 0, 0, 0, 1, 0, 0, 0, 2, 0,
            2, 2, 2, 0, 0, 0, 0, 0, 2, 1, 2,
            2, 2, 2, 0, 0, 0, 0, 0, 2, 2, 2];

        stage_info.stage2 = 
           [0, 0, 0, 1, 0, 0, 2, 2, 0, 0, 0,
            0, 0, 0, 0, 2, 0, 2, 2, 0, 1, 0,
            0, 0, 0, 0, 2, 0, 2, 2, 1, 2, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0,
            0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
            0, 0, 1, 0, 0, 0, 0, 0, 2, 0, 0,
            0, 2, 0, 0, 0, 0, 0, 1, 2, 1, 0,
            0, 0, 1, 0, 0, 2, 0, 2, 0, 2, 0,
            0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0,
            0, 0, 0, 0, 1, 0, 0, 2, 0, 2, 0,
            0, 0, 2, 0, 2, 1, 0, 2, 0, 2, 0];

        stage_info.stage3 = 
           [2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 2,
            0, 2, 0, 0, 0, 0, 0, 0, 1, 0, 2,
            0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 2,
            0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 2,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2,
            0, 1, 1, 1, 0, 0, 0, 2, 0, 0, 1,
            0, 0, 0, 0, 0, 0, 2, 1, 2, 0, 2,
            0, 0, 0, 0, 0, 0, 1, 2, 1, 0, 0,
            0, 0, 0, 0, 0, 0, 2, 1, 2, 0, 0];

        stage_info.stage4 = 
           [0, 1, 0, 0, 1, 2, 1, 0, 0, 0, 0,
            0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1,
            0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2,
            0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0,
            0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 2,
            0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0,
            0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0];

        stage_info.stage5 = 
           [2, 0, 2, 0, 0, 0, 1, 0, 0, 0, 2,
            2, 0, 2, 0, 2, 2, 1, 0, 1, 0, 2,
            0, 0, 2, 0, 2, 2, 0, 0, 2, 0, 0,
            0, 1, 0, 0, 2, 2, 0, 0, 0, 1, 0,
            1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            2, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 1, 0, 1, 0, 2, 0,
            0, 0, 0, 0, 0, 1, 0, 0, 2, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0];

        stage_info.stage6 = 
           [0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0,
            0, 2, 2, 1, 2, 0, 0, 1, 1, 0, 0,
            0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 2, 2, 0, 0, 0, 0, 2, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1,
            0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0,
            0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0];

        stage_info.stage7 = 
           [0, 0, 0, 0, 2, 2, 2, 0, 0, 2, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            0, 2, 2, 0, 0, 2, 0, 0, 0, 0, 2,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2,
            0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0,
            1, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0,
            0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 2, 0, 0, 1, 0, 0, 0, 2, 0,
            2, 2, 2, 0, 0, 0, 0, 0, 2, 2, 2,
            2, 2, 2, 0, 0, 0, 0, 0, 2, 2, 2];

        stage_info.stage8 = 
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        stage_info.stage9 = 
           [2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0,
            2, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 2, 0, 0, 1, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 2,
            2, 0, 0, 0, 2, 0, 1, 0, 0, 0, 1,
            2, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0,
            0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2,
            0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2];

        stage_info.stage10 = 
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0,
            0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0,
            0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        cc.director.loadScene('story');
    }

    load_game(){
        var Ref = firebase.database().ref();
            var user_email = firebase.auth().currentUser.email;
            Ref.once('value')
            .then(function(snapshot) {
                snapshot.forEach(function(childshot) {
                    var childData = childshot.val();
                    if(childData.Email === user_email){
                        cc.log("email:",user_email);
                        cc.log("Global.Playername:",childData.fb_Global.Playername);

                        Global.Playername = childData.fb_Global.Playername;
                        Global.playerblood = childData.fb_Global.playerblood;
                        Global.playerattack = childData.fb_Global.playerattack;
                        Global.playerdefense = childData.fb_Global.playerdefense;
                        Global.playerexp = childData.fb_Global.playerexp;
                        Global.playergold = childData.fb_Global.playergold,
                        Global.playerlevel = childData.fb_Global.playerlevel,
                        Global.playerkey_1 = childData.fb_Global.playerkey_1;
                        Global.playerkey_2 = childData.fb_Global.playerkey_2;
                        Global.player_pos_x = childData.fb_Global.player_pos_x;
                        Global.player_pos_y = childData.fb_Global.player_pos_y;
                        Global.direction = childData.fb_Global.direction;
                        Global.floor_direction = childData.fb_Global.floor_direction;
                        Global.floor_check = childData.fb_Global.floor_check;
                        Global.has_skill_2 = childData.fb_Global.has_skill_2;
                        Global.has_skill_3 = childData.fb_Global.has_skill_3;
                        Global.has_skill_4 = childData.fb_Global.has_skill_4;

                        stage_info.stage0 = childData.fb_stage_info.stage0;
                        stage_info.stage1 = childData.fb_stage_info.stage1;
                        stage_info.stage2 = childData.fb_stage_info.stage2;
                        stage_info.stage3 = childData.fb_stage_info.stage3;
                        stage_info.stage4 = childData.fb_stage_info.stage4;
                        stage_info.stage5 = childData.fb_stage_info.stage5;
                        stage_info.stage6 = childData.fb_stage_info.stage6;
                        stage_info.stage7 = childData.fb_stage_info.stage7;
                        stage_info.stage8 = childData.fb_stage_info.stage8;
                        stage_info.stage9 = childData.fb_stage_info.stage9;
                        stage_info.stage10 = childData.fb_stage_info.stage10;

                        Scene_now.stage = childData.fb_scene_now;

                        cc.director.loadScene(Scene_now.stage);
                    }
                })
            }).catch(e => console.log(e.message));
    }

    music () {
        cc.director.loadScene('music');
    }

    howtoplay () {
        cc.director.loadScene('howtoplay');
    }

    howtoplaynext () {
        cc.director.loadScene('howtoplay2');
    }

    howtoplayback () {
        cc.director.loadScene('Menu');
    }

    jump () {
        cc.director.loadScene('0');
    }

    onLoad () {
        if(Global.is_new){
            cc.find("button").getComponent(cc.Button).interactable = false;
        }
        else{
            var Ref = firebase.database().ref();
            var user_email = firebase.auth().currentUser.email;
            Ref.once('value')
            .then(function(snapshot) {
                snapshot.forEach(function(childshot) {
                    var childData = childshot.val();
                    if(childData.Email === user_email){
                        Global.is_new = childData.Is_new;
                        if(Global.is_new){
                            cc.find("button").getComponent(cc.Button).interactable = false;
                        }
                        else {
                            cc.find("button").getComponent(cc.Button).interactable = true;
                        }
                    }
                })
            }).catch(e => console.log(e.message));
        }

    }

    start () {
        if (cc.audioEngine.isMusicPlaying() == false) {
            Global.bgmID = cc.audioEngine.playMusic(this.bgm, true);
        }
    }

    update () {
        if(this.node.name == 'label')
        {
            if(this.node.y <= 1150)
            {
                this.node.y += 0.7;
            }
            else
            {
                if(cc.director.getScene().name == "story")
                {
                    cc.director.loadScene('0');
                }
                else if(cc.director.getScene().name == "victory")
                {
                    cc.director.loadScene('Menu');
                }
            }
        }
        
    }

}