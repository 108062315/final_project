
const {ccclass, property} = cc._decorator;

import Global = require("./Global");

@ccclass
export default class NewClass extends cc.Component {
    
    @property(cc.Sprite)
    dialogue: cc.Sprite = null;
    @property(cc.Sprite)
    frame: cc.Sprite = null;
    @property(cc.Sprite)
    roleIcon: cc.Sprite = null;
    @property(cc.Sprite)
    roleIcon2: cc.Sprite = null;

    @property(cc.Label)
    nameLabel: cc.Label = null;
    @property(cc.Label)
    textLabel: cc.Label = null;
    @property(cc.Button)
    Next: cc.Button = null;
    @property(cc.Label)
    NextLabel: cc.Label = null;

    
    @property(cc.Button)
    closeIcon: cc.Button = null;
    @property(cc.Sprite)
    closeBackground: cc.Sprite = null;
    @property(cc.Sprite)
    nextBackground: cc.Sprite = null;

    dialogue_show : boolean = false;
    last_dialogue : boolean = false;
    
    // LIFE-CYCLE CALLBACKS:

    count = 0;
    nameString : string = "仙子";
    text : string = "你醒了?";
    a : string = "";
    
    
    onLoad () {
        this.dialogue.node.height = 0;
        this.frame.node.height = 0;
        this.roleIcon.node.height = 0;
        this.closeIcon.node.height = 0;
        this.nameLabel.node.height = 0;
        this.textLabel.node.height = 0;
        this.Next.node.height = 0;
        this.NextLabel.node.height = 0;
        this.roleIcon2.node.height = 0;

        this.closeBackground.node.height = 0;
        this.nextBackground.node.height = 0;

        this.dialogue_show = false;
        
    }

    update(dt){
        
    }

    Next_Page(){
        this.last_dialogue = false;
        //cc.log(this.count);
        //this.count = this.count + 1;
        var tmp = this.count % 6;
        cc.log(tmp);
        
        switch(this.count) {
            case 0:
                this.text = "你是誰?我在哪裡?";
                break;
            case 1:
                this.text = "我是這裡的仙子，剛才你被這裡的小怪打昏了。"
                break;
            case 2:
                this.text = "......公主呢?我是來救公主的。";
                break;
            case 3:
                this.text = "公主還在裡面，我這裡有怪物圖鑑你先拿著。";
                break;
            case 4:
                this.text = "怪物圖鑑?";
                break;
            case 5:
                this.text = "按Z就可以看到怪物圖鑑了!";
                this.last_dialogue = true;
                break;
        }

        this.count = this.count + 1;

        if(this.count % 2 == 1){
            this.nameLabel.node.getComponent(cc.Label).string = "勇者";
            this.roleIcon.node.height = 0;
            this.roleIcon2.node.height = 50;
        }
            
        else{
            this.nameLabel.node.getComponent(cc.Label).string = "仙子";
            this.roleIcon.node.height = 50;
            this.roleIcon2.node.height = 0;
        }
            
        this.textLabel.node.getComponent(cc.Label).string = this.text;
        
        if(this.last_dialogue){
            //alert("last");
            this.Next.node.height = 0;
            this.nextBackground.node.height = 0;
        }
    }

    close_window(){
        this.nameLabel.node.getComponent(cc.Label).string = "";
        this.textLabel.node.getComponent(cc.Label).string = "";
        this.NextLabel.node.getComponent(cc.Label).string = "";

        this.dialogue.node.height = 0;
        this.frame.node.height = 0;
        this.roleIcon.node.height = 0;
        this.roleIcon2.node.height = 0;
        this.closeIcon.node.height = 0;
        
        this.nameLabel.node.height = 0;
        this.textLabel.node.height = 0;
        this.Next.node.height = 0;
        this.NextLabel.node.height = 0;
        this.closeBackground.node.height = 0;
        this.nextBackground.node.height = 0;
        

        this.dialogue_show = false;
    }
    onBeginContact(contact, self, other){
        //cc.log(other.node.name);
        if(other.node.name == "Braver"){
            
            this.nameLabel.node.getComponent(cc.Label).string = this.nameString;
            this.textLabel.node.getComponent(cc.Label).string = this.text;
            this.NextLabel.node.getComponent(cc.Label).string = this.a;

            this.dialogue.node.height = 150;
            this.frame.node.height = 64;
            this.roleIcon.node.height = 50;
            this.closeIcon.node.height = 32;
            this.roleIcon2.node.height = 0;

            this.closeBackground.node.height = 32;
            this.nextBackground.node.height = 32;
            
            this.nameLabel.node.height = 25;
            this.textLabel.node.height = 40;

            this.Next.node.height = 32;
            this.NextLabel.node.height = 30;
            this.count = 1;
            this.dialogue_show = true;
            
        }
    }
}
