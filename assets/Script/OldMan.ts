
const {ccclass, property} = cc._decorator;

import Global = require("./Global");

@ccclass
export default class NewClass extends cc.Component {
    
    @property(cc.Sprite)
    dialogue: cc.Sprite = null;
    @property(cc.Sprite)
    frame: cc.Sprite = null;
    @property(cc.Sprite)
    roleIcon: cc.Sprite = null;
    

    @property(cc.Label)
    nameLabel: cc.Label = null;
    @property(cc.Label)
    textLabel: cc.Label = null;

    @property(cc.Button)
    LevelUp: cc.Button = null;
    @property(cc.Label)
    LevelUpLabel: cc.Label = null;

    @property(cc.Button)
    Skill: cc.Button = null;
    @property(cc.Label)
    SkillLabel: cc.Label = null;

 

    @property(cc.Button)
    Skill_2: cc.Button = null;
    @property(cc.Label)
    SkillLabel_2: cc.Label = null;

    @property(cc.Button)
    Skill_3: cc.Button = null;
    @property(cc.Label)
    SkillLabel_3: cc.Label = null;

    @property(cc.Button)
    Skill_4: cc.Button = null;
    @property(cc.Label)
    SkillLabel_4: cc.Label = null;

    @property(cc.Button)
    closeIcon: cc.Button = null;
    @property(cc.Sprite)
    closeBackground: cc.Sprite = null;

    @property(cc.Sprite)
    LevelUpBackground: cc.Sprite = null;
    @property(cc.Sprite)
    SkillBackground: cc.Sprite = null;
    @property(cc.Sprite)
    Skill2Background: cc.Sprite = null;
    @property(cc.Sprite)
    Skill3Background: cc.Sprite = null;
    @property(cc.Sprite)
    Skill4Background: cc.Sprite = null;

    dialogue_show : boolean = false;
    // LIFE-CYCLE CALLBACKS:

    nameString : string = "老人";
    text : string = "經驗值可以兌換:";
    a : string = "升級";
    d : string = "技能";

    onLoad () {
        this.dialogue.node.height = 0;
        this.frame.node.height = 0;
        this.roleIcon.node.height = 0;
        this.closeIcon.node.height = 0;
        this.nameLabel.node.height = 0;
        this.textLabel.node.height = 0;

        this.LevelUp.node.height = 0;
        this.LevelUpLabel.node.height = 0;
        this.Skill.node.height = 0;
        this.SkillLabel.node.height = 0;
        
        this.Skill_2.node.height = 0;
        this.SkillLabel_2.node.height = 0;
        this.Skill_3.node.height = 0;
        this.SkillLabel_3.node.height = 0;
        this.Skill_4.node.height = 0;
        this.SkillLabel_4.node.height = 0;

        this.closeBackground.node.height = 0;
        this.LevelUpBackground.node.height = 0;
        this.SkillBackground.node.height = 0;
        this.Skill2Background.node.height = 0;
        this.Skill3Background.node.height = 0;
        this.Skill4Background.node.height = 0;

        this.dialogue_show = false;
        
    }

    update(dt){
        
    }

    Level_Up(){
        if(Global.playerexp >= 100){
            alert("Level Up!");
            
            Global.playerdefense = Global.playerdefense + Global.playerlevel*10;
            Global.playerattack = Global.playerattack + Global.playerlevel*10;
            Global.playerexp = Global.playerexp - 100;
            Global.playerlevel = Global.playerlevel + 1;
        }
        else{
            alert("經驗不足!!");
        }
    }

    Get_Skill(){

            this.textLabel.node.getComponent(cc.Label).string = "經驗值可以兌換以下任一技能";
            this.LevelUpLabel.node.getComponent(cc.Label).string = "";
            this.SkillLabel.node.getComponent(cc.Label).string = "";
            this.SkillLabel_2.node.getComponent(cc.Label).string = " 孤狼的咆哮";
            this.SkillLabel_3.node.getComponent(cc.Label).string = "  光明束縛 ";
            this.SkillLabel_4.node.getComponent(cc.Label).string = " 雷神天明閃";

            this.LevelUp.node.height = 0;
            this.LevelUpLabel.node.height = 0;
            this.Skill.node.height = 0;
            this.SkillLabel.node.height = 0;
            
            this.Skill2Background.node.height = 30;
            this.Skill3Background.node.height = 30;
            this.Skill4Background.node.height = 30;

            this.Skill_2.node.height = 30;
            this.SkillLabel_2.node.height = 30;
            this.Skill_3.node.height = 30;
            this.SkillLabel_3.node.height = 30;
            this.Skill_4.node.height = 30;
            this.SkillLabel_4.node.height = 30;
        
    }

    Get_Skill_2(){
        if(Global.playerexp >= 50)
        {
            alert('獲得技能:孤狼的咆哮');
            Global.has_skill_2 = true;
            Global.playerexp = Global.playerexp - 50;
        }
        else
        {
            alert("經驗不足!! 需要50exp");
        }
        //alert(Global.playerexp);
    }
    Get_Skill_3(){
        if(Global.playerexp >= 100)
        {
            alert('獲得技能:光明束縛');
            Global.has_skill_3 = true;
            Global.playerexp = Global.playerexp - 100;
        }
        else
        {
            alert("經驗不足!! 需要100exp");
        }
    }
    Get_Skill_4(){
        if(Global.playerexp >= 200)
        {
            alert('獲得技能:雷神天明閃');
            Global.has_skill_4 = true;
            Global.playerexp = Global.playerexp - 200;
        }
        else
        {
            alert("經驗不足!! 需要200exp");
        }
    }

    close_window(){
        this.nameLabel.node.getComponent(cc.Label).string = "";
        this.textLabel.node.getComponent(cc.Label).string = "";
        this.LevelUpLabel.node.getComponent(cc.Label).string = "";
        this.SkillLabel.node.getComponent(cc.Label).string = "";
        this.SkillLabel_2.node.getComponent(cc.Label).string = "";
        this.SkillLabel_3.node.getComponent(cc.Label).string = "";
        this.SkillLabel_4.node.getComponent(cc.Label).string = "";

        this.dialogue.node.height = 0;
        this.frame.node.height = 0;
        this.roleIcon.node.height = 0;
        this.closeIcon.node.height = 0;
        
        this.nameLabel.node.height = 0;
        this.textLabel.node.height = 0;
        this.LevelUp.node.height = 0;
        this.LevelUpLabel.node.height = 0;
        this.Skill.node.height = 0;
        this.SkillLabel.node.height = 0;

        this.Skill_2.node.height = 0;
        this.SkillLabel_2.node.height = 0;
        this.Skill_3.node.height = 0;
        this.SkillLabel_3.node.height = 0;
        this.Skill_4.node.height = 0;
        this.SkillLabel_4.node.height = 0;
        
        this.closeBackground.node.height = 0;
        this.LevelUpBackground.node.height = 0;
        this.SkillBackground.node.height = 0;
        this.Skill2Background.node.height = 0;
        this.Skill3Background.node.height = 0;
        this.Skill4Background.node.height = 0;

        this.dialogue_show = false;
    }
    onBeginContact(contact, self, other){
        //cc.log(other.node.name);
        if(other.node.name == "Braver"){
            //alert(other.node.name);
            
            this.nameLabel.node.getComponent(cc.Label).string = this.nameString;
            this.textLabel.node.getComponent(cc.Label).string = this.text;
            this.LevelUpLabel.node.getComponent(cc.Label).string = this.a;
            this.SkillLabel.node.getComponent(cc.Label).string = this.d;

            this.dialogue.node.height = 150;
            this.frame.node.height = 64;
            this.roleIcon.node.height = 50;
            this.closeIcon.node.height = 32;
            

            this.closeBackground.node.height = 32;
            this.LevelUpBackground.node.height = 30;
            this.SkillBackground.node.height = 30;

            this.nameLabel.node.height = 25;
            this.textLabel.node.height = 50;
            this.LevelUp.node.height = 30;
            this.LevelUpLabel.node.height = 30;
            this.Skill.node.height = 30;
            this.SkillLabel.node.height = 30;
            
            this.dialogue_show = true;
        }
    }
}
